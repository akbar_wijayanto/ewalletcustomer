angular.module('starter.factory', ['ui.router', 'ngCordova', 'ngStorage'])

.factory('redirectInterceptor', function($location, $q, $localStorage) {
	return function(promise) {
		promise.then(
			function(response) {
				if (typeof response.data === 'object') {
					if (response.data.status.code == 2007) {
						delete $localStorage.usname;
						delete $localStorage.userid;
						delete $localStorage.account;
						delete $localStorage.balance;
						delete $localStorage.token;
						$location.path("/login");
					}
				}
				return response;
			},
			function(response) {
				return $q.reject(response);
			}
		);
		return promise;
	};
})

.factory('servGlobal', function($ionicPlatform, $state, $ionicLoading, $cordovaNetwork, $cordovaFile, $rootScope, $localStorage, popup) {
	return{
		/*
		checkSessionGlobal: function (){
			if (window.localStorage.getItem("appcid")!=null){
				$state.go('login'); 
			}else{
				$state.go('sign'); //sign
			}
		},*/
		checkSessionApp: function (){
			if ($localStorage.usname==null){
				$state.go('login'); 
			}
		},
		sessionTimeout: function (){
			popup.alert("session_timeout");
			delete $localStorage.usname;
			delete $localStorage.userid;
			delete $localStorage.account;
			delete $localStorage.balance;
			delete $localStorage.token;
			$state.go("login");
		},
		loadingShow: function(){
			$ionicLoading.show({
				template: "<p class='load-icon-left' style='font-family: 'Trebuchet MS', Helvetica, sans-serif'>Loading...<ion-spinner icon='lines'/></p>"
			});
		},
		loadingHide: function () {
			$ionicLoading.hide();
		},
		loadAuthShow: function(){
			$ionicLoading.show({
				template: "<p class='load-icon-left' style='font-family: 'Trebuchet MS', Helvetica, sans-serif'>Authenticating...<ion-spinner icon='lines'/></p>"
			});
		},
		loadDownloadShow: function(){
			$ionicLoading.show({
				template: "<p class='load-icon-left' style='font-family: 'Trebuchet MS', Helvetica, sans-serif'>Downloading... <ion-spinner icon='lines'/></p>"
			});
		},
		checkConnection:function(){
			var type = $cordovaNetwork.getNetwork();
			var isOnline = $cordovaNetwork.isOnline();
			var isOffline = $cordovaNetwork.isOffline();
			alert("Type : "+type);
			alert("Is Online : "+isOnline);
			alert("Is Offline : "+isOffline);
			document.addEventListener("online", check, false);
			document.addEventListener("offline", checkoff, false);
			function check(){ alert("Internet"); }
			function checkoff(){ alert("No Internet");}
			// listen for Online event
			$rootScope.$on('$cordovaNetwork:online', function(event, networkState){
				var onlineState = networkState;
			})
			// listen for Offline event
			$rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
				var offlineState = networkState;
			})
		},
		checkFile:function(){
			var base="http://10.2.49.39:8787/mobile-webconsole/apps";
			var data={'endpoint':[
				{id:"login",url:base+"/retailAdapter/loginCustomer"},
				{id:"activation",url:base+"/retailAdapter/activation"},
				{id:"profile",url:base+"/retailAdapter/profile"},
				{id:"setting",url:base+"/retailAdapter/updateUserPreference"},
				{id:"sellers",url:base+"/retailAdapter/getAllSeller"},
				{id:"products",url:base+"/retailAdapter/getItemBySeller"},
				{id:"inqOrder",url:base+"/retailAdapter/checkAccountAmount"},
				{id:"order",url:base+"/retailAdapter/order"},
				{id:"orders",url:base+"/retailAdapter/getOrderList"},
				{id:"histories",url:base+"/retailAdapter/getTransactionHistory"},
				{id:"checkOrder",url:base+"/retailAdapter/validasiOrder"},
				{id:"processOrder",url:base+"/retailAdapter/processOrder"},
				{id:"feedback",url:base+"/retailAdapter/createFeedback"},
				{id:"getSetting",url:base+"/retailAdapter/getUserPreference"}
			]};
			/*var createFile = function(){
				result = CryptoJS.AES.encrypt(JSON.stringify(data), "123456");
				$ionicPlatform.ready(function() {
					$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas_ep.json",String(result), true);
				});
				
			};*/
			/*$ionicPlatform.ready(function() {
				$cordovaFile.checkDir(cordova.file.dataDirectory, "JSON").then(createFile(),
					function(error){
						$cordovaFile.createDir(cordova.file.dataDirectory, "JSON", false).then(createFile());
					}
				)
			})*/
			
		}
	}
})

.factory('popup', function($ionicPopup) {
	return {
		alert : function(text) {
			$ionicPopup.alert({
				title: 'I-Pay',
				template: "{{'"+text+"'|translate}}",
				okType: 'button-energized'
			});
		},
		alert2 : function(text) {
			$ionicPopup.alert({
				title: 'I-Pay',
				template: text,
				okType: 'button-energized'
			});
		},
		success : function(text) {
			$ionicPopup.alert({
				title: 'I-Pay',
				template: "{{'"+text+"'|translate}}",
				okType: 'button-positive'
			});
		}
	}
})

.factory('timestamp',function($filter){
	return{
		show:function(){
			var ref = Date.now();
			var tgl = $filter('date')(ref, 'yyyy-MM-ddTHH:mm:ssZ');
			var tgl_tot=tgl.length;
			var blkng=tgl_tot-2;
			var left=tgl.substr(0,blkng);
			var lenleft=left.length;
			var final_tgl=tgl.substr(lenleft,tgl_tot);
			return(left+":"+final_tgl);
		},
		refnum:function(){
			var ref=0;
			ref=((((Math.random()*10000)+1)+Date.now())*(((Math.random()*10000)+1)+((Math.random()*10000)+1)))/((Math.random()*10000)+1);
			ref=ref.toString().substr(0,8);
			return ref;
		}
	}
})

.factory('jsonOp',function($cordovaFile){
	return{
		getJson:function(file){
			return $cordovaFile.readAsText(cordova.file.dataDirectory, "JSON/"+file).then(
				function(success){
					var decrypted = CryptoJS.AES.decrypt(success, "123456");
					return{
						'isi':JSON.parse(decrypted.toString(CryptoJS.enc.Utf8))
					};
				}
			);
		}
	}
})

.factory('dataRequest', function($http, $cordovaDevice, $localStorage, timestamp, jsonOp, popup, servGlobal, $rootScope) {
	var header = {'Content-Type':'application/json'};
	var method = 'POST';
	var url = null;
	var body = null;
	var endpoint = null;
	
	var deviceOS = null;
	var deviceID = null;
	var osVersion = null;
	var appId = '1';
	var appVersion = '2.0.0';
	var baseUrl = "http://10.2.49.39:8787/mobile-webconsole/apps/retailAdapter/";
	return{
		logIn:function(data){
				url = baseUrl+"loginCustomer";
				body = {
					"metadata" : {
						"datetime" : timestamp.show(),
						"deviceId" : deviceID,
						"devicePlatform" : deviceOS,
						"deviceOSVersion" : osVersion,
						"appId" : appId,
						"appVersion" : appVersion,
						"latitude" : "",
						"longitude" : ""
					},
					"body" : {
						"username" : data.username,
						"password" : data.password
					}
				};
				header = {'Content-Type':'application/json'};
				servGlobal.loadingShow();
				return	$http({method: method, url: url, data:body, headers:header}).then(
					function(response){
						servGlobal.loadingHide();
						/*if(!response.headers("X-AUTH-TOKEN")){
						}
						else{
							$localStorage.token = response.headers("X-AUTH-TOKEN");
						}*/
						return {
							'status' : response.status,
							'data' : response.data
						}
					},
					function(httpError){
						servGlobal.loadingHide();
						if(httpError.status == 0){
							popup.alert("error_no_response");
						}
						else{
							popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
						}
					}
				);
			
		},
		getBalance:function(username){
			url = baseUrl+"accountInformation";
			body = {
				"body" : {
					"username" : username
				}
			};
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					return {
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == 0){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			);
		},
		activate:function(username, password){
			url = endpoint[1].url;
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"username" : username,
					"password" : password,
					"uniqueKey" : ""
				}
			};
			header = {'Content-Type':'application/json'};
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					return {
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == 0){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			);
		},
		getProfile: function () {
			url = endpoint[2].url;
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"username" : $localStorage.usname
				}
			};
			header = {
					'Content-Type':'application/json',
					'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return {
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		setLimitAmount: function (check, limit) {
			url = endpoint[3].url;
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"accountNumber": $localStorage.account,
					"isPayroll": check,
					"limitAmount":	limit
				}
			};
			header = {
					'Content-Type':'application/json',
					'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return {
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		getSellers: function () {
			//url = endpoint[4].url;
			url = baseUrl+"getAllSeller";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var sellers = [];
					var data = {};
					if(!response.data.body.GetAllSellerResponse[0].sellers){
						return{
							'status' : response.status,
							'data' : response.data
						}
					}
					else{
						if(response.data.body.GetAllSellerResponse[0].sellers.hasOwnProperty('0')){
							console.log("seller ::: " + response.data.body.GetAllSellerResponse[0].sellers[0]);
							for(var i=0; i<response.data.body.GetAllSellerResponse[0].sellers.length; i++){
								data.sellerId = response.data.body.GetAllSellerResponse[0].sellers[i].sellerId._text;
								data.sellerName = response.data.body.GetAllSellerResponse[0].sellers[i].sellerName._text;
								data.phoneNumber = response.data.body.GetAllSellerResponse[0].sellers[i].phoneNumber._text;
								data.desc = response.data.body.GetAllSellerResponse[0].sellers[i].descriptions._text;									
								sellers.push(data);
								data={};
							}
						}
						else{
							data.sellerId = response.data.body.GetAllSellerResponse[0].sellers.sellerId._text;
							data.sellerName = response.data.body.GetAllSellerResponse[0].sellers.sellerName._text;
							data.phoneNumber = response.data.body.GetAllSellerResponse[0].sellers.phoneNumber._text;
							data.desc = response.data.body.GetAllSellerResponse[0].sellers.descriptions._text;								
							sellers.push(data);
							data={};
						}
						return{
							'status' : response.status,
							'data' : response.data,
							'sellers' : sellers
						}
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		getProducts: function (merchantId) {
			url = baseUrl+"getItemBySeller";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"sellerId": merchantId
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var products = [];
					var data = {};
					if(!response.data.body.GetItemBySellerIdResponse[0].items){
						return{
							'status' : response.status,
							'data' : response.data
						}
					}
					else{
						if(response.data.body.GetItemBySellerIdResponse[0].items.constructor === Array){
							for(var i=0; i<response.data.body.GetItemBySellerIdResponse[0].items.length; i++){
								data.itemId = response.data.body.GetItemBySellerIdResponse[0].items[i].itemId._text;
								data.itemName = response.data.body.GetItemBySellerIdResponse[0].items[i].itemName._text;
								data.itemType = response.data.body.GetItemBySellerIdResponse[0].items[i].itemType._text;	
								data.price = response.data.body.GetItemBySellerIdResponse[0].items[i].sellingPrice._text;
								data.desc = response.data.body.GetItemBySellerIdResponse[0].items[i].description._text;
								data.qrCode = response.data.body.GetItemBySellerIdResponse[0].items[i].qrCode._text;
								data.image = response.data.body.GetItemBySellerIdResponse[0].items[i].image._text;
								data.qty = 0;
								products.push(data);
								data={};
							}
						}
						else{
							data.itemId = response.data.body.GetItemBySellerIdResponse[0].items.itemId._text;
							data.itemName = response.data.body.GetItemBySellerIdResponse[0].items.itemName._text;
							data.itemType = response.data.body.GetItemBySellerIdResponse[0].items.itemType._text;	
							data.price = response.data.body.GetItemBySellerIdResponse[0].items.sellingPrice._text;
							data.desc = response.data.body.GetItemBySellerIdResponse[0].items.description._text;
							data.qrCode = response.data.body.GetItemBySellerIdResponse[0].items.qrCode._text;
							data.image = response.data.body.GetItemBySellerIdResponse[0].items.image._text;
							data.qty = 0;
							products.push(data);
							data={};
						}
						return{
							'status' : response.status,
							'data' : response.data,
							'products' : products
						}
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		inquiryOrder: function (userId, totalAmount) {
			//url = endpoint[6].url;
			url = baseUrl+"checkAccountAmount";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"userId": userId,
					"totalAmount": totalAmount
				}
			};

			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		order: function (userId, itemId, note) {
			//url = endpoint[7].url;
			url = baseUrl+"order";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"userId": userId,
					"itemsOrder": itemId,
					"notes": note
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status' : response.status,
						'data' : response.data
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		getOrders: function () {
			//url = endpoint[8].url;
			url = baseUrl+"getOrderList";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"username": $rootScope.loginData.username._text
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var orders = [];
					var data = {};
					if(response.data.status.code == 200){
						if(response.data.body.GetOrderListResponse[0].order){
							if(response.data.body.GetOrderListResponse[0].order.constructor === Array){
								for(var i=0; i<response.data.body.GetOrderListResponse[0].order.length; i++){
									data.orderId = response.data.body.GetOrderListResponse[0].order[i].orderId._text;
									data.orderStatus = response.data.body.GetOrderListResponse[0].order[i].status._text;
									data.sellerId = response.data.body.GetOrderListResponse[0].order[i].sellerId._text;	
									data.sellerName = response.data.body.GetOrderListResponse[0].order[i].sellerName._text;	
									data.orderTime = response.data.body.GetOrderListResponse[0].order[i].orderDate._text;
									data.items = response.data.body.GetOrderListResponse[0].order[i].items;
									data.notes = response.data.body.GetOrderListResponse[0].order[i].notes._text;
									orders.push(data);
									data={};
								}
							}
							else{
								data.orderId = response.data.body.GetOrderListResponse[0].order.orderId._text;
								data.orderStatus = response.data.body.GetOrderListResponse[0].order.status._text;
								data.sellerId = response.data.body.GetOrderListResponse[0].order.sellerId._text;	
								data.sellerName = response.data.body.GetOrderListResponse[0].order.sellerName._text;	
								data.orderTime = response.data.body.GetOrderListResponse[0].order.orderDate._text;
								data.items = response.data.body.GetOrderListResponse[0].order.items;
								data.notes = response.data.body.GetOrderListResponse[0].order.notes._text;
								orders.push(data);
								data={};
							}
							return{
								'status' : response.status,
								'data' : response.data,
								'orders' : orders
							}
						}
						else{
							return{
								'status': 123456
							}
						}
					}
					else{
						return{
							'status' : response.status,
							'data' : response.data
						}
					}
				},
				function(httpError){
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		getHistory: function () {
			// url = endpoint[9].url;
			url = baseUrl+"getTransactionHistory";
			method = 'POST';
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"username" : $localStorage.usname
				}
			};
			header = {
					'Content-Type':'application/json',
					'X-AUTH-TOKEN': $localStorage.token
			};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var histories = [];
					var data = {};
					if(!response.data.body){
						return{
							'status' : response.status,
							'data' : response.data
						}
					}
					else{
						if(response.data.body.GetTransactionHistoryResponse[0].transaction.constructor === Array){
							for(var i=0; i<response.data.body.GetTransactionHistoryResponse[0].transaction.length; i++){
								data.transId = response.data.body.GetTransactionHistoryResponse[0].transaction[i].transactionId._text;
								data.transDate = response.data.body.GetTransactionHistoryResponse[0].transaction[i].transactionDate._text;
								data.sellerName = response.data.body.GetTransactionHistoryResponse[0].transaction[i].sellerName._text;	
								data.amount = response.data.body.GetTransactionHistoryResponse[0].transaction[i].amount._text;
								data.status = response.data.body.GetTransactionHistoryResponse[0].transaction[i].status._text;
								histories.push(data);
								data={};
							}
						}
						else{
							data.transId = response.data.body.GetTransactionHistoryResponse[0].transaction.transactionId._text;
							data.transDate = response.data.body.GetTransactionHistoryResponse[0].transaction.transactionDate._text;
							data.sellerName = response.data.body.GetTransactionHistoryResponse[0].transaction.sellerName._text;	
							data.amount = response.data.body.GetTransactionHistoryResponse[0].transaction.amount._text;
							data.status = response.data.body.GetTransactionHistoryResponse[0].transaction.status._text;
							histories.push(data);
							data={};
						}
						return{
							'status' : response.status,
							'data' : response.data,
							'histories' : histories
						}
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			)
		},
		validateOrder:function(seller, order){
			// url = endpoint[10].url;
			url = baseUrl+"validasiOrder";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"sellerId"	:	seller,
					"orderId"	:	order
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		processOrder:function(order){
			// url = endpoint[11].url;
			url = baseUrl+"processOrder";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"transactionId":order,
					"status":"3"
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		sendFeedback:function(fb){
			var tipe;
			if(fb.id == 0){
				tipe = 1;
			}
			else {
				tipe = 0;
			}
			// url = endpoint[12].url;
			url = baseUrl+"createFeedback";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"username" : $localStorage.usname,
					"message" : fb.comment,
					"rating" : fb.rate,
					"type" : tipe, 
					"sellerId" : fb.id
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		getLimitAmount:function(){
			// url = endpoint[13].url;
			url = baseUrl+"getUserPreference";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"username" : $localStorage.usname
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					if(httpError.status == '0'){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},

		doRegister : function(data){
			url = baseUrl+"register";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"fullName" 		: data.fullName,
					"phoneNumber" 	: data.phoneNumber,
					"email" 		: data.email,
					"username" 		: data.username,
					"password" 		: data.password,
					"pin"			: data.pin
				}
			};
			header = {'Content-Type':'application/json'};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();

					var registRes = response.data.body.RegistrationResponse;

					var registRes = {
						"fullName"	: registRes.fullName._text,
						"phoneNumber"	: registRes.phoneNumber._text,
						"email"	: registRes.email._text,
						"username"	: registRes.username._text				
					}
					/*if(!response.headers("X-AUTH-TOKEN")){
					}
					else{
						$localStorage.token = response.headers("X-AUTH-TOKEN");
					}*/
					return {
						'status' : response.data.status,
						'data' : registRes
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == 0){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			);
		},

		createTransfer : function(data){
			url = baseUrl+"createTransfer";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"accountFrom" 	: data.accountFrom,
					"accountTo" 	: data.accountTo,
					"amount" 		: data.amount
				}
			};
			header = {'Content-Type':'application/json'};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();

					var createRes = response.data.body.CreateTransferResponse;
					
					var createRes = {
						"accountFrom"		: createRes.accountFrom._text,
						"accountFromName"	: createRes.accountFromName._text,
						"accountTo"			: createRes.accountTo._text,
						"accountToName"		: createRes.accountToName._text,
						"amount"			: createRes.amount._text,
						"reference"			: createRes.reference._text				
					}
					
					return {
						'status' : response.data.status,
						'data' : createRes
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == 0){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			);
		},

		confirmTransfer : function(data){
			url = baseUrl+"confirmTransfer";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body" : {
					"reference" 	: data.reference,
					"pin" 			: data.pin
				}
			};
			header = {'Content-Type':'application/json'};
			servGlobal.loadingShow();
			return	$http({method: method, url: url, data:body, headers:header}).then(
				function(response){
					servGlobal.loadingHide();

					var confirmRes = response.data.body.ConfirmTransferResponse;
					
					var confirmRes = {
						"reference"		: confirmRes.reference._text,
						"transactionId"	: confirmRes.transactionId._text				
					}

					return {
						'status' : response.data.status,
						'data' : confirmRes
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == 0){
						popup.alert("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+ JSON.stringify(httpError.data));
					}
				}
			);
		}
		
	}
})