angular.module('starter.controllers', ['ionic', 'ngCordova', 'ngStorage', 'starter.factory'])
	
.controller('LoginCtrl',function($scope, $state, $rootScope, $ionicModal, $ionicPopup, $localStorage, $cordovaFileTransfer, popup, dataRequest, servGlobal) {
	if($localStorage.usname){
		$rootScope.savedUser = $localStorage.usname;
	}
	if($localStorage.saveLogin){
		if($localStorage.saveLogin == 'yes') $rootScope.savedSet = true;
		else $rootScope.savedSet = false;
	}
	$scope.login = function(login_data){
		if(!login_data || !login_data.username || !login_data.password || login_data.username == '' || login_data.password == ''){
			popup.alert('error_login');
		}
		else{
			dataRequest.logIn(login_data).then(function(response){
				console.log(response);
				if(response.status == 12345){
					popup.alert("info_first_install");
				}
				else if(response.status == 200){
					$rootScope.loginData = angular.copy(response.data.body.LoginResponse.user); 
					$state.go("home");
				}
			})
		}
	}

	$scope.toRegistration = function(){
		$state.go("registration");
	}
})

.controller('ActivateCtrl', function($scope, $state, $localStorage, popup, dataRequest) {
	$scope.active = function(){
		var username = $localStorage.usname;
		var password = $localStorage.psword;
		delete $localStorage.usname;
		delete $localStorage.psword;
		dataRequest.activate(username, password).then(function(response){
			if(response.status == 200 && response.data.status.code == 200){
				if(response.data.body.ActivationResponse.status[0]._text.match(/911.*/)){
					popup.alert("error_acc_active"); 
					$state.go('login');
				}
				else if(response.data.body.ActivationResponse.status[0]._text.match(/910.*/)){
					popup.alert("success_send_email");
					$state.go('login');
				}
				else{
					popup.alert2("UNKNOWN Error");
					$state.go('login');
				}
			}
		});
	};
})

.controller('HomeCtrl', function ($scope, $state, $rootScope, $localStorage, $ionicModal, popup, servGlobal, dataRequest) {	
	dataRequest.getBalance($rootScope.loginData.username._text).then(function(response){
		$rootScope.balanceData = angular.copy(response.data.body.AccountInformationResponse); 
	});

	$scope.refreshBalance = function(){
		dataRequest.getBalance($rootScope.loginData.username._text).then(function(response){
			$rootScope.balanceData = angular.copy(response.data.body.AccountInformationResponse); 
		}).finally(function(response){
			$scope.$broadcast('scroll.refreshComplete');
		});
	}

	$ionicModal.fromTemplateUrl('image-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openModal = function() {
		$scope.modal.show();
	};

	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	//Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});
	// Execute action on hide modal
	$scope.$on('modal.hide', function() {
	// Execute action
	});
	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
	// Execute action
	});
	$scope.$on('modal.shown', function() {
	});

	$scope.showQRCode = function(data) {
		var data = $rootScope.loginData.qrCode._text;
		$scope.imageSrc = data;
		$scope.openModal();
	}
	$scope.logout = function(){
		$rootScope.profile = null;
		if($localStorage.saveLogin == 'no'){
			$rootScope.savedSet = false;
			$rootScope.savedUser = null;
			delete $localStorage.usname;
		}
		else{
			$rootScope.savedUser = $localStorage.usname;
			$rootScope.savedSet = true;
		}
		delete $localStorage.userid;
		delete $localStorage.account;
		delete $localStorage.balance;
		delete $localStorage.token;
		$state.go("login");
	};
})

.controller('HistoryCtrl', function($scope, $state, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	dataRequest.getHistory().then(function(response){
		if(response.data.status.code == 2007){
			servGlobal.sessionTimeout();
		}
		else{
			$scope.historyData = response.histories;
		}
	});
	$scope.back = function(){
		$state.go("home");
	}
})

.controller('MerchantCtrl', function ($scope, $state, $rootScope, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	dataRequest.getSellers().then(function(response){
		if(response.data.status.code == 2007){
			//servGlobal.sessionTimeout();
		}
		else{
			$scope.sellerData = response.sellers;
		}
	});
	
	$scope.back = function(){
		$state.go("home");
	}
	
	$scope.showProducts = function(merchantName, merchantId){
		$rootScope.seller = merchantName;
		$rootScope.productData = null;
		dataRequest.getProducts(merchantId).then(function(response){
			if(response.data.status.code == 2007){
				//servGlobal.sessionTimeout();
			}
			else{
				$rootScope.productData = angular.copy(response.products); 
				$state.go('products');
			}
		});
	}
})

.controller('ProductCtrl', function ($scope, $state, $rootScope, popup, servGlobal) {
	//servGlobal.checkSessionApp();
	var cartItems = [];
	$scope.addToCart = function(item, product){
		if(product.qty<99){
			product.qty +=1;
			var hasItem = false;
			for (var index in cartItems) {
				if(cartItems[index].id == item.id){
					cartItems[index].qty = cartItems[index].qty + 1;
					var hasItem = true;
					break;
				}
			}
			if(hasItem == false){
				cartItems.push(item);
			}
		}
		else{
			popup.alert("error_max_items");
		}
	}
	$scope.delFromCart = function(item, product){
		product.qty -=1;
		var hasItem = false;
		for (var index in cartItems) {
			if(cartItems[index].id == item.id){
				if(cartItems[index].qty > 1){
					cartItems[index].qty = cartItems[index].qty - 1;
					var hasItem = true;
					break;
				}
				else{
					cartItems.splice(index, 1);
				}
				var hasItem = true;
				break;
			}
		}
	}
	$scope.checkoutInq = function(){
		$rootScope.cart = null;
		if (cartItems[0] != null) {
			$rootScope.cart = cartItems;
			cartItems = [];
			$state.go('checkout');
		}
		else{
			popup.alert("error_no_item");
		}
	}
	$scope.back = function(){
		$rootScope.cart = null;
		$state.go("merchants");
	}
})

.controller('CheckoutCtrl', function ($scope, $state, $rootScope, $localStorage, $ionicPopup, popup, dataRequest, servGlobal) {
	//servGlobal.checkSessionApp();
	$scope.getSumPrice = function(item){
		var total = 0;
		harga = parseInt(item.price);
		jumlah = parseInt(item.qty);
		total = harga * jumlah;
		return total;
	}
	$scope.getTotal = function(items){
		$scope.grandTotal = 0;
		if(items.length == null){
			hargaTotal = parseInt(items.price);
			jumlahTotal = parseInt(items.qty);
			$scope.grandTotal = hargaTotal * jumlahTotal;
		}
		else{
			for(var i = 0; i < items.length; i++){
				hargaTotal = parseInt(items[i].price);
				jumlahTotal = parseInt(items[i].qty);
				$scope.grandTotal += hargaTotal * jumlahTotal;
			}
		}
		return $scope.grandTotal;
	}
	$scope.checkoutPost = function(){
		var note = document.getElementById('notes').value;
		var userid = $rootScope.loginData.id._text;
		var items = [];
		for(var j = 0; j<$rootScope.cart.length; j++){
			items.push({
				"itemId" : $rootScope.cart[j].id,
				"quantity" : $rootScope.cart[j].qty
			});
		}
		var totalAmount = $scope.grandTotal;
		dataRequest.inquiryOrder(userid, totalAmount).then(function(response){
			if(response.data.body.CheckAccountAmountResponse.responseCode == null){
				popup.alert2("UNKNOWN ERROR.");
				$state.go("home");
			}
			else{
				if(response.data.body.CheckAccountAmountResponse.responseCode._text == 0){
					dataRequest.order(userid, items, note).then(function(respon){
						/*
						$rootScope.orderId = respon.data.body.OrderResponse.transactionId[0]._text;
						$rootScope.orderNotes = note;*/
						if(respon.data.body.OrderResponse.transactionId[0]._text){
							$state.go("order");
						}
						else{
							popup.alert2("Error");
						}
					});	
				}
				else if(response.data.body.CheckAccountAmountResponse.responseCode._text == 101){
					var confirmOrder = $ionicPopup.confirm({
						title: 'I-Pay',
						template: "{{'confirm_continue_order'|translate}}"
					});
					confirmOrder.then(function(res) {
						if(res) {
							dataRequest.order(userid, items, note).then(function(respon){
								/*$rootScope.orderId = respon.data.body.OrderResponse.transactionId[0]._text;
								$rootScope.orderNotes = note;
								$state.go("checkout_finish");*/
								if(respon.data.body.OrderResponse.transactionId[0]._text){
									$state.go("order");
								}
								else{
									popup.alert2("Error");
								}
							});	
						} 
						else {
							popup.alert("error_no_balance");
							$state.go("home");
						}
					});
				}
				else if(response.data.body.CheckAccountAmountResponse.responseCode._text == 102 || response.data.body.CheckAccountAmountResponse.responseCode._text == 103){
					popup.alert("error_no_balance");
					$state.go("home");
				}
				else{
					popup.alert2("UNKNOWN ERROR.");
					$state.go("home");
				}
			}
		});				
	}
	$scope.back = function(){
		$rootScope.cart = null;
		$state.go("merchants");
	}
})
/*
.controller('CheckoutFinCtrl', function ($scope, $state, $rootScope, popup, dataRequest, servGlobal) {
	//servGlobal.checkSessionApp();
	$scope.getSumPrice = function(item){
		var total = 0;
		harga = parseInt(item.price);
		jumlah = parseInt(item.qty);
		total = harga * jumlah;
		return total;
	}
	$scope.getTotal = function(items){
		var grandTotal = 0;
		if(items.length == null){
			hargaTotal = parseInt(items.price);
			jumlahTotal = parseInt(items.qty);
			grandTotal = hargaTotal * jumlahTotal;
		}
		else{
			for(var i = 0; i < items.length; i++){
				hargaTotal = parseInt(items[i].price);
				jumlahTotal = parseInt(items[i].qty);
				grandTotal += hargaTotal * jumlahTotal;
			}
		}
		return grandTotal;
	}
	$scope.back = function(){
		$rootScope.productData = null;
		$rootScope.cart = null;
		$rootScope.seller = null;
		$state.go("home");
	}
})
*/
.controller('OrderCtrl', function ($scope, $state, $rootScope, $ionicPopup, $cordovaBarcodeScanner, $localStorage, popup, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	if(!$scope.orderData){
		servGlobal.loadingShow();
		dataRequest.getOrders().then(function(response){
			servGlobal.loadingHide();
			if(response.status == 123456){
				popup.alert("info_no_order");
				$scope.orderData = null;
			}
			else if(response.data.status.code == 2007){
				servGlobal.sessionTimeout();
			}
			else{
				$scope.orderData = response.orders;					
			}
		});
	}
	$scope.refreshOrder = function(){
		dataRequest.getOrders().then(function(response){
			if(response.status == 123456){
				popup.alert("info_no_order");
				$scope.orderData = null;
			}
			else if(response.data.status.code == 2007){
				servGlobal.sessionTimeout();
			}
			else{
				$scope.orderData = response.orders;					
			}
		}).finally(function(response){
			$scope.$broadcast('scroll.refreshComplete');
		});
	}
	$scope.getSumPrice = function(quantity, price){
		var total = 0;
		harga = parseInt(price);
		jumlah = parseInt(quantity);
		total = harga * jumlah;
		return total;
	}
	$scope.getTotal = function(orders){
		var grandTotal = 0;
		if(orders.items.length == null){
			hargaTotal = parseInt(orders.items.sellingPrice._text);
			jumlahTotal = parseInt(orders.items.quantity._text);
			grandTotal = hargaTotal * jumlahTotal;
		}
		else{
			for(var i = 0; i < orders.items.length; i++){
				hargaTotal = parseInt(orders.items[i].sellingPrice._text);
				jumlahTotal = parseInt(orders.items[i].quantity._text);
				grandTotal += hargaTotal * jumlahTotal;
			}
		}
		return grandTotal;
	}
	$scope.confirmOrder = function(order){
		var idOrder = order.orderId;
		var dataOrder = order;
		/*var confirmDeliver = $ionicPopup.confirm({
			title: 'I-Pay',
			template: "{{'warn_confirm_deliver'|translate}}"
		});
		confirmDeliver.then(function(res) {
			if(res) {*/
				dataRequest.processOrder(idOrder).then(function(response){
					if(response.status == 200 && response.data.status.code == 200){
						popup.alert("success_confirm_order");
						var arrayOrder = [];
						if(!$localStorage.arr_order){
							arrayOrder.push(dataOrder);
							$localStorage.arr_order = JSON.stringify(arrayOrder);
						}
						else{
							arrayOrder = JSON.parse($localStorage.arr_order);
							if(arrayOrder.length == 10){
								arrayOrder.splice(0, 1);
							}
							arrayOrder.push(dataOrder);
							$localStorage.arr_order = JSON.stringify(arrayOrder);
						}
						$rootScope.orderFbData = JSON.parse($localStorage.arr_order);
						dataRequest.getOrders().then(function(response){
							if(response.status == 123456){
								$scope.orderData = null;
							}
							else if(response.data.status.code == 2007){
								servGlobal.sessionTimeout();
							}
							else{
								$scope.orderData = response.orders;					
							}	
						});
					}
					else{
						popup.alert2("Error");
					}
				})
/*			}
		});*/
	}
	$scope.back = function(){
		$state.go("home");
	}
})

.controller('OrderFbCtrl', function ($scope, $state, $rootScope, $localStorage, dataRequest, servGlobal) {
	//servGlobal.checkSessionApp();
	if($localStorage.arr_order){
		$rootScope.orderFbData = JSON.parse($localStorage.arr_order);
	}
	$scope.back = function(){
		$state.go("feedback");
	}
	$scope.getTotal = function(order){
		var total = 0;
		if(order.items.length == null){
			harga = parseInt(order.items.sellingPrice._text);
			jumlah = parseInt(order.items.quantity._text);
			total = harga * jumlah;
		}
		else{
			for(var i = 0; i < order.items.length; i++){
				harga = parseInt(order.items[i].sellingPrice._text);
				jumlah = parseInt(order.items[i].quantity._text);
				total += harga * jumlah;
			}
		}
		return total;
	}
	$scope.addFeedBack = function(order){
		$rootScope.fb_data = order;
		$state.go("order_fb_create");
	}
})

.controller('CreateOrderFbCtrl', function ($scope, $state, $localStorage, $rootScope, popup, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	$scope.sendFb = function(fb){
		if(!fb.rate || fb.rate == null || fb.rate == 0){
			popup.alert("error_rate_feedback");
		}
		else{
			dataRequest.sendFeedback(fb).then(function(response){
				if(response.data.status.code == 2007){
					servGlobal.sessionTimeout();
				}
				else{		
					var list_orderfb = [];
					list_orderfb = JSON.parse($localStorage.arr_order);
					if(list_orderfb.length <= 1){
						delete $localStorage.arr_order;
						$rootScope.orderFbData = null;
					}
					else{
						for (var i = 0; i < list_orderfb.length; i++) {
							if(list_orderfb[i].orderId == fb.orderId){
								list_orderfb.splice(i, 1);
								break;
							}
						}
						$localStorage.arr_order = JSON.stringify(list_orderfb);
						$rootScope.orderFbData = list_orderfb;	
					}
					popup.alert("success_send_feedback");
					$state.go("order_fb");
				}
			});
		}
	}
	$scope.back = function(){
		$state.go("order_fb");
	}
})

.controller('ServiceFbCtrl', function ($scope, $state, popup, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	dataRequest.getSellers().then(function(response){
		if(response.data.status.code == 2007){
			servGlobal.sessionTimeout();
		}
		else{
			$scope.sellerData = response.sellers;
		}
	});
	$scope.sendFb = function(fb){
		if(!fb.rate || fb.rate == null || fb.rate == 0){
			popup.alert("error_rate_feedback");
		}
		else{
			dataRequest.sendFeedback(fb).then(function(response){
				if(response.data.status.code == 2007){
					servGlobal.sessionTimeout();
				}
				else{
					popup.alert("success_send_feedback");
					$state.go("feedback");
				}
			});
		}
	}
	$scope.back = function(){
		$state.go("feedback");
	}
})

.controller('SettingsCtrl', function($scope, $rootScope, servGlobal) {
	//servGlobal.checkSessionApp();
})

.controller('LanguageCtrl', function($scope, $translate, $state, popup, servGlobal) {
	//servGlobal.checkSessionApp();
	$scope.changeLang = function(lang){
		$translate.use(lang);
		popup.alert('success_lang_change');
	}
	$scope.back = function(){
		$state.go("settings");
	}
})

.controller('AccountCtrl', function($scope, $ionicPopup, $state, popup, servGlobal, dataRequest) {
	//servGlobal.checkSessionApp();
	var initLimit = 250000;
	dataRequest.getLimitAmount().then(function(response){
		if(response.data.status.code == 2007){
			servGlobal.sessionTimeout();
		}
		else{
			if(response.data.body.GetUserPreferenceResponse[0].allowedPayroll._text == 'true'){
				$scope.check = true;
			}
			else{
				$scope.check = false;
			}
			$scope.limit = parseInt(response.data.body.GetUserPreferenceResponse[0].limitPayroll._text);
		}
	});
	$scope.saveAccount = function(isCheck, limitVal){
		var check = isCheck;
		var limit = limitVal;
		
		var confirmPopup = $ionicPopup.confirm({
			title: 'I-Pay',
			template: "{{'warn_check_sure'|translate}}"
		});
		
		var newLimit = initLimit - limit;
		confirmPopup.then(function(res) {
			if(res) {
				dataRequest.setLimitAmount(check, newLimit).then(function(response){
					if(response.data.status.code == 2007){
						servGlobal.sessionTimeout();
					}
					else {
						popup.success("success_set_acc");
					}
				});
			}
		});
	}
	$scope.back = function(){
		$state.go("settings");
	}
})

.controller('RegistrationCtrl', function($scope, $rootScope, $state, popup, dataRequest) {

	var data = {};

	$scope.doRegist = function(data){
		if(data.password !== data.confirmPass){
			popup.alert('error_password');
		}
		else{
			dataRequest.doRegister(data).then(function(response){
				if(response.status.code != 200 || response.status.code != "200") {
					popup.alert('failed_regist');
				} else {
					popup.success('success_reg');
				}
				$state.go("login");
				console.log(response);
			})
		}
	}

	$scope.back = function(){
		$state.go("login");
	}
	//servGlobal.checkSessionApp();
})
.controller('TransferCtrl', function($scope, $rootScope, $cordovaBarcodeScanner, $state, popup, dataRequest) {

		$scope.data = {};
		$scope.data.qrCode = "081299039695";

		$scope.doScan = function(){
			$cordovaBarcodeScanner.scan().then(function(barcodeData){
				$scope.data.qrCode = barcodeData.text;
			}, function(error) {
				popup.alert("error_barcode");
			});
			
		}

		$scope.doSubmit = function(data){
			var data = {
				"accountFrom"	: $rootScope.loginData.username._text,
				"accountTo"		: $scope.data.qrCode,
				"amount"		: $scope.data.amount
			}

			dataRequest.createTransfer(data).then(function(response){
				if(response.status.code != 200 || response.status.code != "200") {
					popup.alert('failed_regist');
				} else {
					$rootScope.createRes = response;
					$state.go("confirmTransfer");
				}
				
				console.log(response);
			})
		}
	
		$scope.back = function(){
			$state.go("home");
		}
		//servGlobal.checkSessionApp();
	})

	.controller('ConfirmCtrl', function($scope, $rootScope, $cordovaBarcodeScanner, $state, popup, dataRequest, $stateParams) {
		
		$scope.data = $rootScope.createRes.data;

		$scope.doConfirm = function(data){
			var data = {
				"reference"	: $scope.data.reference,
				"pin"		: $scope.data.pin
			}

			dataRequest.confirmTransfer(data).then(function(response){
				if(response.status.code != 200 || response.status.code != "200") {
					popup.alert('failed_regist');
				} else {
					$rootScope.confirmRes = response;
					$state.go("successTransfer");
				}
			})
		}
	
		$scope.back = function(){
			$state.go("home");
		}
		//servGlobal.checkSessionApp();
	})

	.controller('SuccessCtrl', function($scope, $rootScope, $cordovaBarcodeScanner, $state, popup, dataRequest, $stateParams) {
		
		$scope.data = $rootScope.confirmRes.data;
		$scope.success = $rootScope.createRes.data;

		if($scope.data.reference != null) {
			$scope.data.status = "SUCCESS";
		}
	
		$scope.back = function(){
			$state.go("home");
		}
		//servGlobal.checkSessionApp();
	})

;