angular.module('starter', ['ionic', 'ngCordova', 'ngCookies', 'ngStorage', 'starter.controllers', 'starter.factory', 'starter.directives', 'pascalprecht.translate'])

.run(function($ionicPlatform, $ionicPopup, $localStorage, servGlobal, dataRequest) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
	servGlobal.checkFile();
	//dataRequest.getServices();
  });
  
  $ionicPlatform.registerBackButtonAction(function(event) {
	  if (true) { // your check here
		$ionicPopup.confirm({
			title: 'I-Pay',
			template: 'Are you sure you want to exit?'
		}).then(function(res) {
			if (res) {
				delete $localStorage.username;
				delete $localStorage.userid;
				delete $localStorage.account;
				delete $localStorage.token;
				ionic.Platform.exitApp();
			}
		})
	 }
  }, 100);
})

.config(function($httpProvider, $stateProvider, $urlRouterProvider, $translateProvider, $ionicConfigProvider) {
	$ionicConfigProvider.views.maxCache(0);
	$ionicConfigProvider.views.transition('none');
	$translateProvider.useStaticFilesLoader({
		prefix: 'lang/',
        suffix: '.json'
    });
	$translateProvider.preferredLanguage('en_US');
    $translateProvider.useLocalStorage();
		
	$stateProvider
        .state('login', {
            url: "/login",
			templateUrl: "login.html",
			controller: "LoginCtrl"
        })
		.state('activate', {
            url: "/activate",
			templateUrl: "activate.html",
            controller: "ActivateCtrl"
        })
        .state('home', {
            url: "/home",
            templateUrl: "home.html",
            controller: "HomeCtrl"
        })
        .state('merchants', {
            url: "/home/shopping/merchants",
            templateUrl: "merchants.html",
            controller: "MerchantCtrl"
        })
		.state('products', {
            url: "/home/shopping/products",
            templateUrl: "products.html",
            controller: "ProductCtrl"
        })
		.state('checkout', {
            url: "/home/shopping/checkout",
            templateUrl: "checkout.html",
            controller: "CheckoutCtrl"
        })
		.state('checkout_finish', {
            url: "/home/shopping/finish",
            templateUrl: "checkout_finish.html",
            controller: "CheckoutFinCtrl"
        })
	    .state('history', {
            url: "/home/history",
            templateUrl: "history.html",
            controller: "HistoryCtrl"
        })
        .state('order', {
            url: "/home/order",
            templateUrl: "order.html",
            controller: "OrderCtrl"
        })
		.state('feedback', {
            url: "/home/feedback",
            templateUrl: "feedback.html"
        })
		.state('order_fb', {
            url: "/home/feedback/order",
            templateUrl: "order_fb.html",
            controller: "OrderFbCtrl"
        })
		.state('order_fb_create', {
            url: "/home/feedback/order/create",
            templateUrl: "order_fb_create.html",
            controller: "CreateOrderFbCtrl"
        })
		.state('service_fb', {
            url: "/home/feedback/service",
            templateUrl: "service_fb.html",
            controller: "ServiceFbCtrl"
        })
		.state('settings', {
            url: "/home/settings",
			templateUrl: "settings.html",
			controller: "SettingsCtrl"
        })
		.state('language', {
            url: "/home/settings/language",
            templateUrl: "language.html",
            controller: 'LanguageCtrl'
        })
		.state('account', {
            url: "/home/settings/account",
            templateUrl: "account.html",
            controller: 'AccountCtrl'
        })
        .state('registration', {
            url: "/registration",
            templateUrl: "registration.html",
            controller: 'RegistrationCtrl'
        })
        .state('transfer', {
            url: "/home/transfer",
            templateUrl: "transfer.html",
            controller: 'TransferCtrl'
        })
        .state('confirmTransfer', {
            url: "/home/transfer/confirmTransfer",
            templateUrl: "confirmTransfer.html",
            controller: 'ConfirmCtrl'
        })
        .state('successTransfer', {
            url: "/home/transfer/confirmTransfer",
            templateUrl: "successTransfer.html",
            controller: 'SuccessCtrl'
        })
        ;
    $urlRouterProvider.otherwise('login');
	$httpProvider.interceptors.push('redirectInterceptor');
});
