angular.module('starter.directives', ['ionic', 'ngCordova', 'starter.controllers'])
	
	.directive('inputStars', function () {
        var directive = {
            restrict: 'EA',
            replace: true,
            template: '<ul ng-class="listClass">' +
            '<li ng-touch="paintStars($index)" ng-mouseenter="paintStars($index, true)" ng-mouseleave="unpaintStars($index, false)" ng-repeat="item in items track by $index">' +
            '<i  ng-class="getClass($index)" ng-click="setValue($index, $event)"></i>' +
            '</li>' +
            '</ul>',
            require: 'ngModel',
            scope: true,
            link: link
        };

        return directive;
		
        function link(scope, element, attrs, ngModelCtrl) {
            scope.items = new Array(+attrs.max);
            var emptyIcon = attrs.iconEmpty || 'ion-android-star-outline';
            var iconHover = attrs.iconHover || 'angular-input-stars-hover';
            var fullIcon = attrs.iconFull || 'ion-android-star';
            var iconBase = attrs.iconBase || 'icon';
            scope.listClass = attrs.listClass || 'angular-input-stars';
            scope.readonly  = ! (attrs.readonly === undefined);

            ngModelCtrl.$render = function () {
                scope.last_value = ngModelCtrl.$viewValue || 0;
            };

            scope.getClass = function (index) {
                return index >= scope.last_value ? iconBase + ' ' + emptyIcon : iconBase + ' ' + fullIcon + ' active ';
            };

            scope.unpaintStars = function ($index, hover) {
                scope.paintStars(scope.last_value - 1, hover);
            };

            scope.paintStars = function ($index, hover) {
                //ignore painting, if readonly
                if (scope.readonly) {
                    return;
                }
                var items = element.find('li').find('i');
                for (var index = 0; index < items.length; index++) {
                    var $star = angular.element(items[index]);
                    if ($index >= index) {
                        $star.removeClass(emptyIcon);
                        $star.addClass(fullIcon);
                        $star.addClass('active');
                        $star.addClass(iconHover);
                    }
					else {
                        $star.removeClass(fullIcon);
                        $star.removeClass('active');
                        $star.removeClass(iconHover);
                        $star.addClass(emptyIcon);
                    }
                }
                !hover && items.removeClass(iconHover);
            };

            scope.setValue = function (index, e) {
                //ignore painting
                if (scope.readonly) {
                    return;
                }
                var star = e.target;
                if (e.pageX < star.getBoundingClientRect().left + star.offsetWidth / 2) {
                    scope.last_value = index + 1;
                } 
				else {
                    scope.last_value = index + 1;
                }
                ngModelCtrl.$setViewValue(scope.last_value);
            };
        }
    })
	
	.directive('counter', function() {
		return {
			restrict: 'A',
			scope: { value: '=value' },
			template: '<button class="button button-positive button-small" style="display: inline-block;" ng-click="minus()"><i class="icon ion-minus"></i></button><input type="number" placeholder="0" class="amount" ng-model="value" ng-change="changed()" ng-readonly="readonly"><button class="button button-positive button-small" ng-click="plus()"><i class="icon ion-plus"></i></button>',
			link: function( scope , element , attributes ) {
				// Make sure the value attribute is not missing.
				if ( angular.isUndefined(scope.value) ) {
					throw "Missing the value attribute on the counter directive.";
				}
            
				var min = angular.isUndefined(attributes.min) ? null : parseInt(attributes.min);
				var max = angular.isUndefined(attributes.max) ? null : parseInt(attributes.max);
				var step = angular.isUndefined(attributes.step) ? 1 : parseInt(attributes.step);
            
				//element.addClass('counter-container');
            
				// If the 'editable' attribute is set, we will make the field editable.
				scope.readonly = angular.isUndefined(attributes.editable) ? true : false;
            
				/**
				 * Sets the value as an integer.
				 */
				var setValue = function( val ) {
					scope.value = parseInt( val );
				}
            
				// Set the value initially, as an integer.
				setValue( scope.value );
            
				/**
				 * Decrement the value and make sure we stay within the limits, if defined.
				 */
				scope.minus = function() {
					if ( min && (scope.value <= min || scope.value - step <= min) || min === 0 && scope.value < 1 ) {
						setValue( min );
						return false;
					}
						setValue( scope.value - step );
				};
            
				/**
				 * Increment the value and make sure we stay within the limits, if defined.
				 */
				scope.plus = function() {
					if ( max && (scope.value >= max || scope.value + step >= max) ) {
						setValue( max );
						return false;
					}
					setValue( scope.value + step );
				};
            
				/**
				 * This is only triggered when the field is manually edited by the user.
				 * Where we can perform some validation and make sure that they enter the
				 * correct values from within the restrictions.
				 */
				scope.changed = function() {
                // If the user decides to delete the number, we will set it to 0.
					if ( !scope.value ) setValue( 0 );
                
					// Check if what's typed is numeric or if it has any letters.
					if ( /[0-9]/.test(scope.value) ) {
						setValue( scope.value );
					}
					else {
						setValue( scope.min );
					}
                
					// If a minimum is set, let's make sure we're within the limit.
					if ( min && (scope.value <= min || scope.value - step <= min) ) {
						setValue( min );
						return false;
					}
                
					// If a maximum is set, let's make sure we're within the limit.
					if ( max && (scope.value >= max || scope.value + step >= max) ) {
						setValue( max );
						return false;
					}
                
					// Re-set the value as an integer.
					setValue( scope.value );
				};
			}
		}
	});
